//
//  POI.swift
//  Mondriaan
//
//  Created by Zsolt Magyi on 3/19/20.
//  Copyright © 2020 tl0zs. All rights reserved.
//

import Foundation
import UIKit

class POI {
    private var imageUrl: String?
    private var imageThumbnail: Data?
    let address: String
    let coordinates: Coordinate
    let city: String
    let name: String
    let description: String
    
    struct Coordinate {
        let latitude: Double
        let longitude: Double
    }

    init(placeDict: [String: AnyObject]) {
        address = placeDict["address"] as! String
        city = placeDict["city"] as! String
        imageUrl = placeDict["image"] as? String
        name = placeDict["name"] as! String
        description = placeDict["description"] as! String
        
        let lat = placeDict["coordinates"]?["latitude"] as? Double ?? 0.0
        let lon = placeDict["coordinates"]?["longitude"] as? Double ?? 0.0
        
        coordinates = Coordinate(latitude: lat, longitude: lon)
    }
    
    func setThumbnail(data: Data) {
        imageThumbnail = data
    }
    
    func getThumbnail() -> UIImage? {
        return imageThumbnail != nil ? UIImage(data: imageThumbnail!) : nil
    }
    
    func getImageUrl() -> String? {
        return imageUrl
    }
    
    func removeImageUrl() {
        imageUrl = nil
    }
}
