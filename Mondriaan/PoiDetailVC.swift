//
//  PoiDetailVC.swift
//  Mondriaan
//
//  Created by Zsolt Magyi on 3/19/20.
//  Copyright © 2020 tl0zs. All rights reserved.
//

import UIKit
import GoogleMaps

class PoiDetailVC: UIViewController {
    static let storyboardID = "poiDetailVC"
    var pois: [POI]!

    @IBOutlet weak var googleView: GMSMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initPois()
        showTitleCity()
    }
}

extension PoiDetailVC {
    fileprivate func showTitleCity() {
        var title = ""
        if pois.count == 1 {
            title = pois.last!.name
        } else {
            title = pois.last!.city
        }
    
        let label = UILabel()
        label.text = title
        label.font = UIFont.systemFont(ofSize: 20, weight: .bold)
        label.textColor = UIColor(red:0.48, green:0.13, blue:0.91, alpha:1.00)
        label.contentMode = .center
        
        navigationItem.titleView = label
    }

    fileprivate func initPois() {
        for poi in pois {
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2D(latitude: poi.coordinates.latitude, longitude: poi.coordinates.longitude)
            marker.map = googleView
        }
        
        if pois.count == 1 {
            let camera = GMSCameraPosition.camera(withLatitude: pois.last!.coordinates.latitude, longitude: pois.last!.coordinates.longitude, zoom: 15)
                  googleView.camera = camera
        } else {
            let camera = GMSCameraPosition.camera(withLatitude: pois.last!.coordinates.latitude, longitude: pois.last!.coordinates.longitude, zoom: 12)
            googleView.camera = camera
        }
    }
}
