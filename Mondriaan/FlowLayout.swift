//
//  FlowLayout.swift
//  Mondriaan
//
//  Created by Zsolt Magyi on 3/19/20.
//  Copyright © 2020 tl0zs. All rights reserved.
//

import Foundation
import UIKit

class FlowLayout: UICollectionViewFlowLayout {
    override init() {
        super.init()
    }
    
    init(with frame: CGRect) {
        super.init()
        
        let headerHeight: CGFloat = 50
        let gap: CGFloat = 10
        let width = UIScreen.main.bounds.width / 2 - gap * 3
        
        itemSize = CGSize(width: width, height: width)
        scrollDirection = .vertical
        minimumInteritemSpacing = gap
        minimumLineSpacing = gap
        sectionInset = UIEdgeInsets(top: 10, left: gap, bottom: 10, right: gap)
    
        headerReferenceSize = CGSize(width: UIScreen.main.bounds.width, height: headerHeight)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
