//
//  PoiCell.swift
//  Mondriaan
//
//  Created by Zsolt Magyi on 3/19/20.
//  Copyright © 2020 tl0zs. All rights reserved.
//

import UIKit

class PoiCell: UICollectionViewCell {
    static let cellId = "poiCell"
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        layer.cornerRadius = 5
        layer.masksToBounds = true
    }
}
