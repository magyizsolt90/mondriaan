//
//  Constants.swift
//  Mondriaan
//
//  Created by Zsolt Magyi on 3/19/20.
//  Copyright © 2020 tl0zs. All rights reserved.
//

import Foundation


struct Constansts {
    struct Images {
        static let mondriaanLogoNavBar = "MondriaanLogo"
        static let backButtonNavBar = "back"
        static let placeholderImage = "placeholder_image"
    }
}
