//
//  NetworkError.swift
//  Mondriaan
//
//  Created by Zsolt Magyi on 3/19/20.
//  Copyright © 2020 Zsolt Magyi. All rights reserved.
//

import Foundation


enum NetworkError: Error {
    case requestError
    case noResponseError
    case invalidSessionError
    case invalidJsonFomatError
}
