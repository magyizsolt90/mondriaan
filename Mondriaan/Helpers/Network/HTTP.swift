//
//  HTTP.swift
//  Mondriaan
//
//  Created by Zsolt Magyi on 3/19/20.
//  Copyright © 2020 Zsolt Magyi. All rights reserved.
//

import Foundation

enum HTTPTYPE: String {
    case GET
}

struct HTTPHeader {
    struct Keys {
        static let authorization = "Authorization"
    }
}
