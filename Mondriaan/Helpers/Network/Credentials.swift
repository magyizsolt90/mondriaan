//
//  Credentials.swift
//  Mondriaan
//
//  Created by Zsolt Magyi on 3/19/20.
//  Copyright © 2020 tl0zs. All rights reserved.
//

import Foundation


struct Credentials {
    private static let username = "candidate"
    private static let password = "Ahs6aiX7"
    
    static func getEncodedCredentials() -> String {
        let loginString = username + ":" + password
        let loginData = loginString.data(using: String.Encoding.utf8)!
        let base64LoginString = loginData.base64EncodedString()

        return "Basic \(base64LoginString)"
    }
}
