//
//  JSONConverter.swift
//  Mondriaan
//
//  Created by Zsolt Magyi on 3/19/20.
//  Copyright © 2020 Zsolt Magyi. All rights reserved.
//

import Foundation

class JSONConverter {
    static func JSON2Dict(data: Data) -> Dictionary<String, AnyObject>? {
        do {
            let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? Dictionary<String, AnyObject>
            return dict
        } catch let error {
            print("couldnt do the dictionary from data, error: \(error.localizedDescription)")
            return nil
        }
    }
    
    static func JSON2AnyObject(data: Data) -> Any? {
        do {
            let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
            return dict
        } catch let error {
            print("couldnt do the dictionary from data, error: \(error.localizedDescription)")
            return nil
        }
    }
    
    static func dict2JSON(dict: Dictionary<String, AnyObject>) -> Data? {
        do {
            let json =  try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
            return json
        } catch let parseRRRR {
            print("Couldnt parse feedback data to JSON: \(parseRRRR)")
            return nil
        }
    }
}
