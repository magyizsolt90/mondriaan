//
//  Networking.swift
//  Mondriaan
//
//  Created by Zsolt Magyi on 3/19/20.
//  Copyright © 2020 tl0zs. All rights reserved.
//

import Foundation

typealias PoiResult = Result<[[String:AnyObject]], NetworkError>

class Networking {
    static let shared = Networking()
    private init() {}
    
     func downloadPois(completion: @escaping ((PoiResult) -> Void)) {
        guard let request = createRequest(urlStr: URLs.places,
                                          type: .GET,
                                          headers: [HTTPHeader.Keys.authorization: Credentials.getEncodedCredentials()]) else {
            return
        }
        
        
        createSession(request, completion: { (result) in
            switch result {
            case .success(let data):
                if let pois = JSONConverter.JSON2AnyObject(data: data) as? [[String: AnyObject]] {
                    completion(.success(pois))
                } else {
                    completion(.failure(.invalidJsonFomatError))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        })
    }
    
    func downloadThumbnail(urlPath: String, completion: @escaping ((Data?) -> Void)) {
        guard let request = createRequest(urlStr: urlPath, type: .GET) else {
            completion(nil)
            return
        }
        
        createSession(request, completion: { (result) in
            switch result {
            case .success(let data):
                completion(data)
            case .failure(let error):
                print("Couldn't find the image with url: \(error.localizedDescription)")
                completion(nil)
            }
        })
    }
}


//MARK: Helpers
extension Networking {
    fileprivate func createSession(_ request: URLRequest, completion: @escaping ((Result<Data, NetworkError>) -> Void)) {
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: { (data, response, error) in
            if data == nil {
                completion(.failure(.noResponseError))
            } else {
                if let statusCode = self.getStatusCode(response: response) {
                    if (200..<300) ~= statusCode {
                        completion(.success(data!))
                    } else {
                        completion(.failure(.invalidSessionError))
                    }
                } else {
                    completion(.failure(.invalidSessionError))
                }
            }
            
            
        })
        if task.state == .running {
            task.cancel()
        }
        task.resume()
        session.finishTasksAndInvalidate()
    }
    
    
    fileprivate func createRequest(urlStr: String,
                                   type: HTTPTYPE,
                                   body: Dictionary<String, AnyObject>? = nil,
                                   headers: Dictionary<String, String>? = nil,
                                   bodyData: Data? = nil) -> URLRequest? {
        
        let formattedUrl = urlStr.replacingOccurrences(of: " ", with: "%")
        guard let url = URL(string: formattedUrl) else { return nil }
        let request = NSMutableURLRequest(url: url)
        request.httpMethod = type.rawValue

        if headers != nil {
            addHeadersToRequest(request, headers: headers!)
        }

        if body != nil {
            addBodyToRequest(request, body: body!)
        } else if bodyData != nil {
            request.httpBody = bodyData!
        }

        return request as URLRequest
    }
    
    fileprivate func getStatusCode(response: URLResponse?) -> Int? {
        if let httpResponse = response as? HTTPURLResponse {
            return httpResponse.statusCode
        }
        return nil
    }
    
    private func addHeadersToRequest(_ request: NSMutableURLRequest, headers: Dictionary<String, String>) {
        for (key, value) in headers {
            request.addValue(value, forHTTPHeaderField: key)
        }
    }
    
    private func addBodyToRequest(_ request: NSMutableURLRequest, body: Dictionary<String, AnyObject>) {
        let data = JSONConverter.dict2JSON(dict: body)
        request.httpBody = data
    }
}
