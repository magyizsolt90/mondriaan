//
//  URLs.swift
//  Mondriaan
//
//  Created by Zsolt Magyi on 3/19/20.
//  Copyright © 2020 Zsolt Magyi. All rights reserved.
//

import Foundation

struct URLs {
    static let baseURL = "https://candidate.mondriaan.com"
    static let places = baseURL + "/places"
}
