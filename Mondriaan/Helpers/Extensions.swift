//
//  Extensions.swift
//  Mondriaan
//
//  Created by Zsolt Magyi on 3/19/20.
//  Copyright © 2020 tl0zs. All rights reserved.
//

import Foundation
import UIKit

extension String {
    func getLocalizedValue() -> String? {
        return NSLocalizedString(self, comment: "")
    }
}


extension UIAlertController {
    static func showPlainAlert(with message: String, title: String = "Error") -> UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(ok)
        return alert
    }
}
