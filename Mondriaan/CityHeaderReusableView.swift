//
//  CityHeaderReusableView.swift
//  Mondriaan
//
//  Created by Zsolt Magyi on 3/19/20.
//  Copyright © 2020 tl0zs. All rights reserved.
//

import UIKit

protocol CityHeaderDelegate: class {
    func showTheWholeCity(section: Int)
}

class CityHeaderReusableView: UICollectionReusableView {
    static let reuseID = "headerViewId"
    var section: Int!
    weak var delegate: CityHeaderDelegate?
    
    @IBOutlet weak var cityLabel: UILabel!

    @IBAction func shouldShowTheWholeCity(sender: UIButton) {
        delegate?.showTheWholeCity(section: section)
    }
}
