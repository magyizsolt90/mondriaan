//
//  ViewController.swift
//  Mondriaan
//
//  Created by Zsolt Magyi on 3/19/20.
//  Copyright © 2020 tl0zs. All rights reserved.
//

import UIKit

class PoiVC: UIViewController {
    fileprivate var places: [[POI]]?
    fileprivate var logoView: UIImageView?
    fileprivate var refresher: UIRefreshControl!
    
    @IBOutlet weak var poiCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initNavbar()
        downloadPois()
        initCollection()
        initRefreshControl()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        initNavbar()
    }
}

//MARK: Networking
extension PoiVC {
    @objc fileprivate func downloadPois() {
        showLoad()
        
        Networking.shared.downloadPois { [weak self] (poiResults) in
            switch poiResults {
            case .success(let places):
                var pois = [POI]()
                for place in places {
                    let poi = POI(placeDict: place)
                    pois.append(poi)
                }
                
                DispatchQueue.main.async {
                    self?.storePlaces(pois)
                    self?.updateList()
                    self?.hideLoad()
                }
            case .failure(let networkError):
                DispatchQueue.main.async {
                    self?.hideLoad()
                    self?.showError(networkError.localizedDescription)
                }
            }
        }
    }
    
    fileprivate func downloadThumbnailFor(indexPath: IndexPath, url: String) {
        if let location = places?[indexPath.section][indexPath.row] {
            Networking.shared.downloadThumbnail(urlPath: url) { (imgData) in
                if imgData != nil {
                    location.setThumbnail(data: imgData!)
                    DispatchQueue.main.async { [weak self] in
                        self?.poiCollectionView.reloadItems(at: [indexPath])
                    }
                } else {
                    location.removeImageUrl()
                }
            }
        }
    }
    
    private func storePlaces(_ places: [POI]) {
        let groupedPlaces = Dictionary(grouping: places, by: { $0.city })
        self.places = [[POI]]()
        
        for (_, placesArray) in groupedPlaces {
            self.places?.append(placesArray)
        }
    }
}

//MARK: Collection delegates
extension PoiVC: UICollectionViewDelegate, UICollectionViewDataSource, CityHeaderDelegate {
    func showTheWholeCity(section: Int) {
        if let pois = places?[section] {
            if let poiDetailVC = storyboard?.instantiateViewController(withIdentifier: PoiDetailVC.storyboardID) as? PoiDetailVC {
                poiDetailVC.pois = pois
                navigationController?.pushViewController(poiDetailVC, animated: true)
                hideMondriaanView()
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return places != nil ? places![section].count : 0
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        let sections = places != nil ? places!.count : 0
        return sections
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PoiCell.cellId, for: indexPath) as! PoiCell
        let poi = places![indexPath.section][indexPath.row]
        
        cell.descriptionLabel.text = poi.description
        cell.descriptionLabel.sizeToFit()
        cell.nameLabel.text = poi.name
        cell.addressLabel.text = poi.address
        
        if let img = poi.getThumbnail() {
            cell.imageView.image = img
        } else {
            if let url = poi.getImageUrl() {
                downloadThumbnailFor(indexPath: indexPath, url: url)
            } else {
                cell.imageView.image = UIImage(named: Constansts.Images.placeholderImage)
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: CityHeaderReusableView.reuseID, for: indexPath) as! CityHeaderReusableView
        if let poiCity = places?[indexPath.section][indexPath.row].city {
            headerView.cityLabel.text = poiCity
            headerView.section = indexPath.section
            headerView.delegate = self
        }
        
        return headerView
    }
    
    fileprivate func updateList() {
        poiCollectionView.reloadData()
    }
    
    fileprivate func initCollection() {
        poiCollectionView.delegate = self
        poiCollectionView.dataSource = self
        poiCollectionView.collectionViewLayout = FlowLayout(with: view.frame)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        hideMondriaanView()
        if segue.identifier == "showOnePoi" {
            if let path = poiCollectionView.indexPathsForSelectedItems?.last, let singlePoi = places?[path.section][path.row] {
                if let poiDetailVC = segue.destination as? PoiDetailVC {
                    poiDetailVC.pois = [singlePoi]
                }
            }
        }
    }
}

//MARK: Helpers
extension PoiVC {
    fileprivate func initNavbar() {
        if logoView == nil || !navigationController!.navigationBar.subviews.contains(logoView!) {
            guard let logoName = Constansts.Images.mondriaanLogoNavBar.getLocalizedValue() else { return }
            let gap: CGFloat = 20
            logoView = UIImageView(frame: CGRect(x: gap,
                                                 y: 0,
                                                 width: UIScreen.main.bounds.width - gap,
                                                 height: navigationController!.navigationBar.frame.height))
            logoView?.image = UIImage(named: logoName)
            logoView?.contentMode = .left
            navigationController?.navigationBar.addSubview(logoView!)
        }
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(downloadPois))
    }
    
    fileprivate func hideMondriaanView() {
        logoView?.removeFromSuperview()
        logoView = nil
    }
    
    fileprivate func initRefreshControl() {
        refresher = UIRefreshControl()
        refresher.tintColor = .black
        refresher.addTarget(self, action: #selector(downloadPois), for: .valueChanged)
        poiCollectionView.addSubview(refresher)
    }
    
    fileprivate func showLoad() {
        poiCollectionView.refreshControl?.beginRefreshing()
    }
    
    fileprivate func hideLoad() {
        poiCollectionView.refreshControl?.endRefreshing()
    }
    
    fileprivate func showError(_ error: String) {
        let alert = UIAlertController.showPlainAlert(with: error)
        present(alert, animated: true, completion: nil)
    }
}
